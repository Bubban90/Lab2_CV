//
//  TableViewCell.swift
//  CV_AppV3
//
//  Created by Fredrik Bredenius on 2017-10-30.
//  Copyright © 2017 Fredrik Bredenius. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var mainCellLabel: UILabel!
    @IBOutlet weak var mainCellImage: UIImageView!
    @IBOutlet weak var nonMainCellLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
