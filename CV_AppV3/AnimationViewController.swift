//
//  AnimationViewController.swift
//  CV_AppV3
//
//  Created by Fredrik Bredenius on 2017-10-31.
//  Copyright © 2017 Fredrik Bredenius. All rights reserved.
//

import UIKit

class AnimationViewController: UIViewController {
    
    @IBOutlet weak var animationX: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        animationX.constant -= view.bounds.width
        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 3, delay: 0, options: .curveEaseOut, animations: {
            self.animationX.constant += self.view.bounds.width
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
