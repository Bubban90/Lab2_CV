//
//  ViewController.swift
//  CV_AppV3
//
//  Created by Fredrik Bredenius on 2017-10-30.
//  Copyright © 2017 Fredrik Bredenius. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    
    
    @IBOutlet weak var tableView: UITableView!
    
    let sectionOneLabelText = ["Fredrik Bredenius", "111-1234567", "Mail@email.com", "Adressgatan 1, 55614", "Favvo webpage"]
    let jobSectionLabelText = ["JTH Mjukvaruutveckling", "Fläktwoods", "Keolis"]
    let demoText = ["IMBA DEMO!"]
    
    // Returns the amout of sections
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    // Returns the amount of rows in each section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return sectionOneLabelText.count
        case 1:
            return jobSectionLabelText.count
        case 2:
            return demoText.count
        default:
            return 0
        }
    }
    
    // Returns the titles for the sections
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 1:
            return "Work experience and education"
        case 2:
            return "Imba demos"
        default:
            return ""
        }
    }
    
    //creates the cells in each section
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            switch indexPath.row{
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "mainCell", for:indexPath) as! TableViewCell
                cell.mainCellImage.image = UIImage(named: "Panda.jpeg")
                cell.mainCellLabel.text = sectionOneLabelText[indexPath.row]
                return cell
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "nonMainCell", for:indexPath) as! TableViewCell
                cell.nonMainCellLabel.text = sectionOneLabelText[indexPath.row]
                return cell
            }
            
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "nonMainCell", for:indexPath) as! TableViewCell
            cell.nonMainCellLabel.text = jobSectionLabelText[indexPath.row]
            return cell
            
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "nonMainCell", for:indexPath) as! TableViewCell
            cell.nonMainCellLabel.text = demoText[indexPath.row]
            return cell
            
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "nonMainCell", for:indexPath) as! TableViewCell
            return cell
        }
    }
    // Makes the height on automatic
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    // Prepares data for Job section cells
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let indexPath = self.tableView.indexPathForSelectedRow{
            if(indexPath.section == 1){
                let destination = segue.destination as! JobViewController
                if(indexPath.row == 0){
                    destination.setImage = "Panda.jpeg"
                    destination.setJobLabel = "JTH"
                }
                else if(indexPath.row == 1){
                    destination.setImage = "Panda.jpeg"
                    destination.setJobLabel = "Flaktwoods"
                }
                else if(indexPath.row == 2){
                    destination.setImage = "Panda.jpeg"
                    destination.setJobLabel = "keolis"
                }
                else{
                    
                }
            }
            
        }
    }
    
    //Handles all the segues
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.section == 0 && indexPath.row == 1){
            UIApplication.shared.open(URL(string: "tel://\("1111234567")")!)
        }
        else if(indexPath.section == 0 && indexPath.row == 2){
            UIApplication.shared.open(URL(string: "mailto:Mail@email.com")!)
        }
        else if(indexPath.section == 0 && indexPath.row == 3){
            performSegue(withIdentifier: "mapSegue", sender: self)
        }
        else if(indexPath.section == 0 && indexPath.row == 4){
            performSegue(withIdentifier: "webSegue", sender: self)
        }
        else if(indexPath.section == 2 && indexPath.row == 0){
            performSegue(withIdentifier: "animationSegue", sender: self)
        }
        else if (indexPath.section == 1){
            performSegue(withIdentifier: "jobSegue", sender: self)
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

