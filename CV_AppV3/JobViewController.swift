//
//  JobViewController.swift
//  CV_AppV3
//
//  Created by Fredrik Bredenius on 2017-10-30.
//  Copyright © 2017 Fredrik Bredenius. All rights reserved.
//

import UIKit

class JobViewController: UIViewController {

    @IBOutlet weak var jobViewImage: UIImageView!
    @IBOutlet weak var jobViewLabel: UILabel!
    
    var setImage : String = ""
    var setJobLabel : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        jobViewImage.image = UIImage(named: setImage)
        jobViewLabel.text = setJobLabel
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
