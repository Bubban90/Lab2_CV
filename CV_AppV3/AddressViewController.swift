//
//  AddressViewController.swift
//  CV_AppV3
//
//  Created by Fredrik Bredenius on 2017-10-30.
//  Copyright © 2017 Fredrik Bredenius. All rights reserved.
//

import UIKit
import MapKit


class AddressViewController: UIViewController {
    
    
    @IBOutlet weak var mapKit: MKMapView!
    
    
    let homePin = MKPointAnnotation()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let homePoint = CLLocation(latitude: 57.740678, longitude: 14.150082)
        homePin.coordinate = CLLocationCoordinate2D(latitude: 57.740678, longitude: 14.150082)
        mapKit.addAnnotation(homePin)
        centerMapOnLocation(location: homePoint)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    let regionRadius: CLLocationDistance = 1000
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, regionRadius, regionRadius)
        mapKit.setRegion(coordinateRegion, animated: true)
    }

}
